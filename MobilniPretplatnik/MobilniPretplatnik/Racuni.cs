﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilniPretplatnik
{
    public class Racuni : MobilniPretplatnik
    {
        public string BrojPretplatnika;
        public bool StatusRacuna;
        private int IznosRacuna;
        private string MesecRacuna;
        private string[] streamArray;

        public Racuni()
        {

        }
        public Racuni(string BrojPretplatnika, bool StatusRacuna, int IznosRacuna, string MesecRacuna)
        {
            this.BrojPretplatnika = BrojPretplatnika;
            this.StatusRacuna = StatusRacuna;
            this.IznosRacuna = IznosRacuna;
            this.MesecRacuna = MesecRacuna;
        }

        public int GetIznosRacuna()
        {
            return this.IznosRacuna;
        }

        public string GetMesecRacuna()
        {
            return this.MesecRacuna;
        }

        public Racuni Ucitaj(StreamReader sr)
        {
            //Ucitavanje iz fajla
            string line = "";
            if ((line = sr.ReadLine()) != null)
            {
                this.streamArray = line.Split(';');
                this.BrojPretplatnika = this.streamArray[0];
                this.StatusRacuna = bool.Parse(this.streamArray[1]);
                this.IznosRacuna = int.Parse(this.streamArray[2]);
                this.MesecRacuna = this.streamArray[3];

                return this;
            }
            else
            {
                return null;
            }

        }

        public void Close(StreamReader sr)
        {
            sr.Close();
        }

        public void SnimiPlaceneRacune()
        {

        }


    }
}
