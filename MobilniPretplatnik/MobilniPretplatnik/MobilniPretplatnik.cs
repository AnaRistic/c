﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilniPretplatnik
{
    public class MobilniPretplatnik
    {
        public string NazivOperatera;
        public string BrojPretplatnika;
        private string ImePretplatnika;
        private string PrezimePretplatnika;
        private string AdresaPretplatnika;
        private string[] streamArray;

        public MobilniPretplatnik()
        {

        }
        public MobilniPretplatnik(string NazivOperatera, string BrojPretplatnika, string ImePretplatnika, string PrezimePretplatnika, string AdresaPretplatnika)
        {
            this.NazivOperatera = NazivOperatera;
            this.BrojPretplatnika = BrojPretplatnika;
            this.ImePretplatnika = ImePretplatnika;
            this.PrezimePretplatnika = PrezimePretplatnika;
            this.AdresaPretplatnika = AdresaPretplatnika;
        }

        public string GetImePretplatnika()
        {
            return this.ImePretplatnika;
        }

        public string GetPrezimePretplatnika()
        {
            return this.PrezimePretplatnika;
        }

        public string GetAdresaPretplatnika()
        {
            return this.AdresaPretplatnika;
        }

        public MobilniPretplatnik Ucitaj(StreamReader sr)
        {
            string line = "";

            if ((line = sr.ReadLine()) != null)
            {
                this.streamArray = line.Split(';');
                this.NazivOperatera = this.streamArray[0];
                this.BrojPretplatnika = this.streamArray[1];
                this.ImePretplatnika = this.streamArray[2];
                this.PrezimePretplatnika = this.streamArray[3];
                this.AdresaPretplatnika = this.streamArray[4];

                return this;
            }
            else
            {
                return null;
            }
        }

        public void Close(StreamReader sr)
        {
            sr.Close();
        }
    }
}
