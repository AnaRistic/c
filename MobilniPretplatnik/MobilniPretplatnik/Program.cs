﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilniPretplatnik
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int pretplatniciBrojac = 0;
                int racuniBrojac = 0;

                MobilniPretplatnik pomocniPretplatnik = new MobilniPretplatnik();
                MobilniPretplatnik[] pretplatnici = new MobilniPretplatnik[4];

                Racuni pomocniRacun = new Racuni();
                Racuni[] racuni = new Racuni[4];

                StreamReader sr = new StreamReader("MobilniPretplatnici.txt");
                StreamReader sr1 = new StreamReader("Racuni.txt");

                StreamWriter sw = new StreamWriter("placeni.txt");
                StreamWriter sw1 = new StreamWriter("neplaceni.txt");

                while (pomocniPretplatnik.Ucitaj(sr) != null)
                {
                    pretplatnici[pretplatniciBrojac] = new MobilniPretplatnik(pomocniPretplatnik.NazivOperatera, pomocniPretplatnik.BrojPretplatnika, pomocniPretplatnik.GetImePretplatnika(), pomocniPretplatnik.GetPrezimePretplatnika(), pomocniPretplatnik.GetAdresaPretplatnika());
                    pretplatniciBrojac++;
                }

                while (pomocniRacun.Ucitaj(sr1) != null)
                {
                    racuni[racuniBrojac] = new Racuni(pomocniRacun.BrojPretplatnika, pomocniRacun.StatusRacuna, pomocniRacun.GetIznosRacuna(), pomocniRacun.GetMesecRacuna());
                    racuniBrojac++;
                }


                for (int i = 0; i < pretplatniciBrojac; i++)
                {
                    for (int j = 0; j < racuniBrojac; j++)
                    {
                        if (pretplatnici[i].BrojPretplatnika == racuni[j].BrojPretplatnika)
                        {
                            if (racuni[j].StatusRacuna)
                            {
                                //Upisivanje u fajl
                                sw.WriteLine("Operater: " + pretplatnici[i].NazivOperatera + "\n");
                                sw.WriteLine("Broj Pretplatnika: " + pretplatnici[i].BrojPretplatnika + "\n");
                                sw.WriteLine("Ime i prezime: " + pretplatnici[i].GetImePretplatnika() + " " + pretplatnici[i].GetPrezimePretplatnika() + "\n");
                                sw.WriteLine("Adresa: " + pretplatnici[i].GetAdresaPretplatnika() + "\n");
                                sw.WriteLine("Mesec Racuna: " + racuni[j].GetMesecRacuna() + "\n");
                                sw.WriteLine("Iznos Racuna: " + racuni[j].GetIznosRacuna() + "\n");
                                sw.WriteLine("------------------------------------------------");
                            }
                            else
                            {
                                sw1.WriteLine("Operater: " + pretplatnici[i].NazivOperatera + "\n");
                                sw1.WriteLine("Broj Pretplatnika: " + pretplatnici[i].BrojPretplatnika + "\n");
                                sw1.WriteLine("Ime i prezime: " + pretplatnici[i].GetImePretplatnika() + " " + pretplatnici[i].GetPrezimePretplatnika() + "\n");
                                sw1.WriteLine("Adresa: " + pretplatnici[i].GetAdresaPretplatnika() + "\n");
                                sw1.WriteLine("Mesec Racuna: " + racuni[j].GetMesecRacuna() + "\n");
                                sw1.WriteLine("Iznos Racuna: " + racuni[j].GetIznosRacuna() + "\n");
                                sw1.WriteLine("------------------------------------------------");
                            }
                        }
                    }

                }


                pomocniRacun.Close(sr1);
                pomocniPretplatnik.Close(sr);
                sw.Close();
                sw1.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
