﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikl
{
    class Torba : Artikl
    {
        Artikl sadrzaj;

        public Torba(string naziv, float cena, Artikl sadrzaj) : base(naziv, cena)
        {
            this.sadrzaj = sadrzaj;
        }

        public override void showDescription()
        {
            Console.WriteLine("Torba za Laptop racunar");
            base.showDescription();
            Console.WriteLine("Naziv: " + this.sadrzaj.naziv);
            Console.WriteLine("Cena: " + this.sadrzaj.cena);
        }

        public void put(Artikl artiklKojJeUlazniParametar)
        {
            if (this.sadrzaj != null)
            {
                Console.WriteLine("U torbi se vec postoji artikl, morate prvo da izvadite iz torbe a zatim stavite novi ");
            }
            else
            {
                this.sadrzaj = artiklKojJeUlazniParametar;
                Console.WriteLine("Uspesno ste ubacili novi artikl");
            }

        }

        public void remove()
        {
            if (this.sadrzaj != null)
            {
                this.sadrzaj = null;
                Console.WriteLine("Torba je prazna i spremna za ponovno koriscenje");
            }
            else
            {
                Console.WriteLine("Torba je vec prazna i ima mogucnost da se ubaci novi laptop u nju");
            }

        }
    }
}
