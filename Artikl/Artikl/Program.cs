﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikl
{
    class Program
    {
        static void Main(string[] args)
        {
            Artikl dell = new Laptop("Dell", 4500, "opis Dell", false);
            Artikl lenovo = new Laptop("Lenovo", 4600, "opis Lenovo", false);

            Torba torba = new Torba("HP", 1200, dell);

            torba.showDescription();
            //Probati primer pre remove, da se proveri ispravnost uslova.
            torba.remove();

            torba.put(lenovo);
            torba.showDescription();
        }
    }
}
