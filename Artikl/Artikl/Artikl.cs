﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikl
{
    public class Artikl
    {
        public string naziv;
        public float cena;

        public Artikl(string naziv, float cena)
        {
            this.naziv = naziv;
            this.cena = cena;
        }

        public virtual void showDescription()
        {
            Console.WriteLine("Naziv: " + this.naziv);
            Console.WriteLine("Cena: " + this.cena);
        }

        public float getPrice()
        {
            return this.cena;
        }
    }
}
