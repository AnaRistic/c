﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ucenik
{
    public class Program
    {
        static void Main(string[] args)
        {
            Ucenici ucenici = new Ucenici(12);

            ucenici.Ucitaj();
            ucenici.GetUcenikSaMaxBrPoena();
            ucenici.UnesiUcenikaUListu();
            ucenici.SnimiUFile();
        }
    }
}
