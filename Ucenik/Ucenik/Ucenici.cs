﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ucenik
{
    public class Ucenici
    {
        public Ucenik[] nizUcenika;
        public int MaxBrUcenika;
        public int brojac;

        public Ucenici(int MaxBrUcenika)
        {
            this.MaxBrUcenika = MaxBrUcenika;
            this.brojac = 0;

            this.nizUcenika = new Ucenik[MaxBrUcenika];
            for (int i = 0; i < MaxBrUcenika; i++)
            {
                this.nizUcenika[i] = new Ucenik();
            }
        }

        public void Ucitaj()
        {
            string linija = "";
            using (StreamReader sr = new StreamReader("UceniciUnos.txt"))
            {
                while ((linija = sr.ReadLine()) != null)
                {
                    string[] niz = linija.Split(';');

                    this.nizUcenika[brojac].ime = niz[0];
                    this.nizUcenika[brojac].prezime = niz[1];
                    this.nizUcenika[brojac].odeljenje = niz[2];

                    int i;
                    if (int.TryParse(niz[3], out i))
                    {
                        this.nizUcenika[brojac].UkupanBrojBodova = i;
                        brojac++;
                    }
                }
            }
            Console.WriteLine("Podaci su ucitani...");
        }

        public void GetUcenikSaMaxBrPoena()
        {
            int maxBr = 0;
            for (int i = 0; i < nizUcenika.Length; i++)
            {
                if (nizUcenika[i].UkupanBrojBodova > maxBr)
                {
                    maxBr = nizUcenika[i].UkupanBrojBodova;
                }
            }

            foreach (var ucenik in nizUcenika)
            {
                if (ucenik.UkupanBrojBodova == maxBr)
                {
                    Console.WriteLine("Ucenik sa maksimalnim brojem poena je: " + ucenik.ime + " " + ucenik.prezime);
                }
            }
        }

        public void UnesiUcenikaUListu()
        {
            Console.WriteLine("Unesite broj poena: ");

            List<Ucenik> listaUcenika = new List<Ucenik>();
            Ucenik ucenik = new Ucenik();
            int poeni;
            if (int.TryParse(Console.ReadLine(), out poeni))
            {

                for (int i = 0; i < nizUcenika.Length; i++)
                {
                    if (nizUcenika[i].UkupanBrojBodova > poeni)
                    {
                        listaUcenika.Add(nizUcenika[i]);
                        ucenik = nizUcenika[i];
                        
                    }
                }
                Console.WriteLine("Ucenik sa vecim brojem poena od unetog je: " + ucenik.ime + " " + ucenik.prezime);
            }

        }

        public void SnimiUFile()
        {
            using (StreamWriter sw = new StreamWriter("UceniciPrikaz.txt"))
            {
                for (int i = 0; i < this.nizUcenika.Length; i++)
                {
                    sw.WriteLine(nizUcenika[i].ime + ";" + nizUcenika[i].prezime + ";" + nizUcenika[i].odeljenje + ";" + nizUcenika[i].UkupanBrojBodova);
                }
            }
            Console.WriteLine("Podaci su upisani u file \"UceniciPrikaz.txt\"");
        }
    }
}
