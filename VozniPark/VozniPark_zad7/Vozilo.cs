﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark_zad7
{
    public class Vozilo
    {
        public bool stanjeVozila;
        public bool stanjeVrata;
        public int brzina;
        public float potrosnja;
        string[] podaci;

        public Vozilo()
        {

        }

        public Vozilo(bool stanjeVozila, bool stanjeVrata, int brzina, float potrosnja)
        {
            this.stanjeVozila = stanjeVozila;
            this.stanjeVrata = stanjeVrata;
            this.brzina = brzina;
            this.potrosnja = potrosnja;
        }

        public void otvori()
        {
            this.stanjeVrata = true;
        }

        public void prikazi()
        {
            Console.WriteLine("Stanje vrata su " + this.stanjeVrata);
            Console.WriteLine("Stanje vozila je" + this.stanjeVozila);
            Console.WriteLine("Trenutna brzina kojom se vozilo krece je: " + this.brzina);
            Console.WriteLine("Potrosnja automobila je: " + this.potrosnja);
        }

        public Vozilo prikaziManjuPotrosnju(Vozilo a)
        {
            //Demonstracija makroa.
            return this.potrosnja < a.potrosnja ? this : a;
        }

        public void ubrzaj()
        {
            this.brzina += 20;
        }

        public void ugasi()
        {
            this.stanjeVozila = false;
        }

        public void upali()
        {
            this.stanjeVozila = true;
        }

        public void uspori()
        {
            this.brzina -= 10;
        }

        public void zatvori()
        {
            this.stanjeVrata = false;
        }

        public Vozilo ucitaj(StreamReader sr)
        {
            string linija = "";

            if ((linija = sr.ReadLine()) != null)
            {
                this.podaci = linija.Split(',');
                this.stanjeVozila = bool.Parse(this.podaci[0]);
                this.stanjeVrata = bool.Parse(this.podaci[1]);
                this.brzina = int.Parse(this.podaci[2]);
                this.potrosnja = float.Parse(this.podaci[3]);

                return this;
            }
            else
            {
                return null;
            }
        }

        public void Close(StreamReader sr)
        {
            sr.Close();
        }
    }
}
