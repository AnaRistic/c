﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark_zad7
{
    public class Program
    {
        static void Main(string[] args)
        {
            Vozilo pomocnoVozilo = new Vozilo();
            StreamReader sr = new StreamReader("Vozila.txt");

            VozniPark park1 = new VozniPark();

            while (pomocnoVozilo.ucitaj(sr) != null)
            {
                Vozilo vozilo = new Vozilo(pomocnoVozilo.stanjeVozila, pomocnoVozilo.stanjeVrata, pomocnoVozilo.brzina, pomocnoVozilo.potrosnja);
                park1.popuniMatricu(vozilo);
            }

            
            /*Vozilo golf = new Vozilo(true, false, 50, 4);
            Vozilo audi = new Vozilo(true, false, 65, 5);
            Vozilo bmw = new Vozilo(true, false, 57, 3);
            Vozilo lada = new Vozilo(true, true, 22, 11);
            Vozilo jugo = new Vozilo(true, true, 33, 11);

            park1.popuniMatricu(golf);
            park1.popuniMatricu(audi);
            park1.popuniMatricu(bmw);
            park1.popuniMatricu(lada);
            park1.popuniMatricu(jugo);*/

            //Ispis matrice u fajl
            park1.prikaziMatricu();
            park1.upisiUFajl();

            pomocnoVozilo.Close(sr);

            //Cipele nike = new Cipele("zelena", "patike", true);
        }
    }
}
