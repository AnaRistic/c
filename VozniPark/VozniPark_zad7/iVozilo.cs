﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VozniPark_zad7
{
    interface iVozilo
    {
        void upali();
        void ugasi();
        void otvori();
        void zatvori();
        void ubrzaj();
        void uspori();
        void prikazi();
        Vozilo prikaziManjuPotrosnju(Vozilo a);
    }
}
