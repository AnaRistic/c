﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caffe
{
    class Caffe
    {
        private int brojSedecihMesta;
        private int brojStolova;
        private bool[,] zauzetSto = new bool[3, 3];
        private bool[,] nelaceniRacun = new bool[3, 3];
        private float iznosNeplacenihRacuna;
        private float ukupanBaksis;
        private int i;
        private int j;

        public float IznosNeplacenihRacuna
        {
            get
            {
                return this.iznosNeplacenihRacuna;
            }
            set
            {
                this.iznosNeplacenihRacuna = value;
            }
        }

        public float Baksis
        {
            get
            {
                return this.ukupanBaksis;
            }
            set
            {
                ukupanBaksis += value * 0.2f;
            }
        }


        public Caffe(int brojSedecihMesta, int brojStolova)
        {
            this.brojSedecihMesta = brojSedecihMesta;
            this.brojStolova = brojStolova;
        }

        public void zauzmiSto()
        {
            var iDuzina = zauzetSto.GetLength(0);
            var jDuzina = zauzetSto.GetLength(1);

            if (j < jDuzina && i <= iDuzina)
            {
                this.zauzetSto[i, j] = true;
                j++;
            }
            else if (++i < iDuzina)
            {
                j = 0;
                this.zauzetSto[i, j] = true;
                j++;
            }
            else
            {
                Console.WriteLine("Nema slobodnih stolova");
            }
        }

        public void oslobodiSto()
        {
            var iDuzina = zauzetSto.GetLength(0);
            var jDuzina = zauzetSto.GetLength(1);

            if (j < jDuzina && i <= iDuzina)
            {
                this.zauzetSto[i, j] = false;
                j++;
            }
            else if (++i < iDuzina)
            {
                j = 0;
                this.zauzetSto[i, j] = false;
                j++;
            }
            else
            {
                Console.WriteLine("Svi stolovi su prazni");
            }
        }

        public void neplaceniRacun(float iznosRacuna)
        {
            var iDuzina = nelaceniRacun.GetLength(0);
            var jDuzina = nelaceniRacun.GetLength(1);

            if (j < jDuzina && i <= iDuzina)
            {
                this.nelaceniRacun[i, j] = true;
                j++;
            }
            else if (++i < iDuzina)
            {
                j = 0;
                this.nelaceniRacun[i, j] = true;
                j++;
            }
            else
            {
                Console.WriteLine("Nema vise stolova");
            }

            this.IznosNeplacenihRacuna += iznosRacuna;
        }

        public void isplacenRacun(float iznosRacuna)
        {
            var iDuzina = nelaceniRacun.GetLength(0);
            var jDuzina = nelaceniRacun.GetLength(1);

            if (j < jDuzina && i <= iDuzina)
            {
                this.nelaceniRacun[i, j] = false;
                j++;
            }
            else if (++i < iDuzina)
            {
                j = 0;
                this.nelaceniRacun[i, j] = false;
                j++;
            }
            else
            {
                Console.WriteLine("Nema vise stolova");
            }

            this.IznosNeplacenihRacuna -= iznosRacuna;
        }

        public void setBaksis(float iznosRacuna)
        {
            float baksis = iznosRacuna * 0.2f;
            this.ukupanBaksis += baksis;
        }

        public void prikaziInformacijeOStolu()
        {
            for (int i = 0; i < 3; i++)
            {

                for (int j = 0; j < 3; j++)
                {
                    Console.Write(" " + this.zauzetSto[i, j]);
                }

                Console.WriteLine();
            }
        }

        public void neplaceniRacuniPoStolovima()
        {
            for (int i = 0; i < 3; i++)
            {

                for (int j = 0; j < 3; j++)
                {
                    Console.Write(" " + this.nelaceniRacun[i, j]);
                }

                Console.WriteLine();
            }
        }



    }
}
