﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legitimacija
{
    public class Program
    {
        static void Main(string[] args)
        {
            Legitimacija NisExpres = new Legitimacija("Nis Expres", 452);
            Legitimacija Lasta = new Legitimacija("Lasta", 362);

            Lice Pera = new Lice(452, "Pera", "Peric");
            Lice Mika = new Lice(362, "Mika", "Mikic");

            MesecnaKarta kartaA = new MesecnaKarta(452, "Oktobar", 1980);
            MesecnaKarta kartaB = new MesecnaKarta(452, "Novembar", 1990);

            MesecnaKarta kartaC = new MesecnaKarta(362, "Mart", 2400);
            MesecnaKarta kartaD = new MesecnaKarta(362, "April", 2450);

            
        }
    }
}
