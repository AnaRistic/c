﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legitimacija
{
    public class MesecnaKarta : Legitimacija
    {
        private string mesec;
        private int cena;

        public MesecnaKarta(int brojLegitimacije, string mesec, int cena) : base(brojLegitimacije)
        {
            this.mesec = mesec;
            this.cena = cena;
        }

        public void Prikazi()
        {
            base.prikazi();
            Console.WriteLine("Mesec: " + mesec);
            Console.WriteLine("Cena: " + cena);
        }
    }
}
