﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legitimacija
{
    public class Legitimacija
    {
        private string imePrevoznika;
        public int brojLegitimacije;

        public Legitimacija(int brojLegitimacije)
        {
            this.brojLegitimacije = brojLegitimacije;
        }

        public Legitimacija(string imePrevoznika, int brojLegitimacije)
        {
            this.imePrevoznika = imePrevoznika;
            this.brojLegitimacije = brojLegitimacije;
        }

        public void prikazi()
        {
            Console.WriteLine("Ime prevoznika: " + this.imePrevoznika);
        }
    }
}
