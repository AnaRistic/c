﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Legitimacija
{
    public class Lice : Legitimacija
    {
        private string ime;
        private string prezime;

        public Lice(int brojLegitimacije, string ime, string prezime) : base(brojLegitimacije)
        {
            this.ime = ime;
            this.prezime = prezime;
        }

        public void Prikazi()
        {
            Console.WriteLine("Ime: " + ime);
            Console.WriteLine("Prezime: " + prezime);
            base.prikazi();
        }
    }
}
